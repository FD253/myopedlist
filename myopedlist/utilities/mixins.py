from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic.detail import SingleObjectMixin

from ..songs.models import ListEntry


class LoginRequiredMixin(object):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).\
            dispatch(request, *args, **kwargs)


class EntryMixin(LoginRequiredMixin):

    def dispatch(self, request, *args, **kwargs):
        self.entry = get_object_or_404(ListEntry, song_id=kwargs['slug'],
                                       user=request.user)
        return super(EntryMixin, self).dispatch(request, *args, **kwargs)


class PermissionMixin(LoginRequiredMixin, SingleObjectMixin):

    def get_object(self, *args, **kwargs):
        obj = super(PermissionMixin, self).get_object(*args, **kwargs)
        owner = None
        if hasattr(obj, 'user'):
            owner = obj.user
        elif hasattr(obj, 'entry'):
            owner = obj.entry.user

        if not owner == self.request.user:
            raise PermissionDenied

        return obj


class OwnerOnlyMixin(LoginRequiredMixin):

    def check_owner(self, request):
        self.object = self.get_object()
        return request.user==self.object.owner

    def get(self, request, *args, **kwargs):
        if not self.check_owner(request):
            raise PermissionDenied()
        return super(OwnerOnlyMixin, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not self.check_owner(request):
            raise PermissionDenied()
        return super(OwnerOnlyMixin, self).post(request, *args, **kwargs)

