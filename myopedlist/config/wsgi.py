from dj_static import Cling
from django.core.wsgi import get_wsgi_application
from .settings.base import BASE_DIR
from ..utilities.whitenoise_compressor import DjangoCompressWhiteNoise

#application = Cling(get_wsgi_applicaion())

#from whitenoise.django import DjangoWhiteNoise

application = get_wsgi_application()
#application = DjangoWhiteNoise(application)

application = DjangoCompressWhiteNoise(application)

application.add_files(BASE_DIR.ancestor(1).child('staticfiles'), prefix='more-files/')
