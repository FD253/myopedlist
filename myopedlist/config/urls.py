from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from ..songs.views import *
from ..users.views import UserProfileUpdate

urlpatterns = [
    url(r'^(?P<slug>\d+)/entry_create/$',
        ListEntryCreateView.as_view(),
        name='entry_create'),

    url(r'^entry/(?P<pk>\d+)/update/$',
        ListEntryUpdateView.as_view(),
        name='entry_update'),

    url(r'^entry/(?P<pk>\d+)/delete/$',
        ListEntryDeleteView.as_view(),
        name='entry_delete'),

    url(r'^$', home, name='home'),
    url(r'^anime/(?P<slug>[-_\w]+)/$', SongListByAnimeView.as_view(), name='song_list_by_anime'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^user/', include('myopedlist.users.urls', namespace='users')),
    url(r'^oped/', include('myopedlist.songs.urls', namespace='songs')),
    url(r'^votes/', include('qhonuskan_votes.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT}),
    url(r'^list/(?P<slug>[-_\w]+)/$', SongListByUserView.as_view(),
        name='song_list_by_user'),


    url(r'^(?P<slug>\d+)/review/$', SongReviewCreateView.as_view(),
        name='review_create'),
    url(r'^review/(?P<pk>\d+)/update/$', SongReviewUpdateView.as_view(),
        name='review_update'),
    url(r'^review/(?P<pk>\d+)/delete/$', SongReviewDeleteView.as_view(),
        name='review_delete'),


    url(r'^(?P<slug>\d+)/reviews/$', SongReviewBySongView.as_view(),
        name='song_review_by_song'),
    url(r'^(?P<slug>\d+)/$', SongDetailView.as_view(),
        name='song_detail'),

    url(r'^summernote/', include('django_summernote.urls')),
]
