import os

import dj_database_url
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _
from unipath import Path


def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the {} environment variable".format(var_name)
        raise ImproperlyConfigured(error_msg)

BASE_DIR = Path(__file__).ancestor(3)

SECRET_KEY = get_env_variable('DJANGO_SECRET_KEY')


STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder'
]

MEDIA_ROOT = BASE_DIR.child("media")
MEDIA_URL = '/media/'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.humanize',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Third Party
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'imagekit',
    'django_countries',
    'qhonuskan_votes',
    'crispy_forms',
    'django_summernote',
    'storages',
    'social.apps.django_app.default',
    'compressor',
    'friendship',
    # My App
    'myopedlist.users',
    'myopedlist.songs',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR.child("templates"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],

        },
    }
]

STATICFILES_DIRS = [
    BASE_DIR.child('static'),
]

SITE_ID = 1

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'myopedlist.config.urls'

WSGI_APPLICATION = 'myopedlist.config.wsgi.application'

DATABASES = {
    'default': dj_database_url.config()
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

LANGUAGES = [
    ('en', _('English')),
    ('es', _('Spanish')),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

COMPRESS_ENABLED = True
COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile} {outfile}'),
)

CRISPY_TEMPLATE_PACK = 'bootstrap3'

SOCIAL_AUTH_TWITTER_LOGIN_URL = '/account/signup'
SOCIAL_AUTH_LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/'

ACCOUNT_EMAIL_REQUIRED = False
# ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_SIGNUP_FORM_CLASS = 'myopedlist.users.forms.SignupForm'

SUMMERNOTE_CONFIG = {
    'inplacewidget_external_css': (
        '//netdna.bootstrapcdn.com/font-awesome/'
        '4.0.3/css/font-awesome.min.css',
    ),
    'inplacewidget_external_js': (),
}
