from .base import *

DATABASES = {
    'default': dj_database_url.config()
}

DEBUG = False
ALLOWED_HOSTS = ['myopedlist.herokuapp.com']
STATICFILES_DIRS = [
    BASE_DIR.child('static'),
]

if 'COMPRESS_OFFLINE' not in os.environ:
    COMPRESS_OFFLINE = not DEBUG

    AWS_QUERYSTRING_AUTH = False
    AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
    AWS_STORAGE_BUCKET_NAME = get_env_variable('S3_BUCKET_NAME')
    AWS_BUCKET_URL = 'https://{0}.s3.amazonaws.com/'.format(
        AWS_STORAGE_BUCKET_NAME)

    MEDIA_URL = AWS_BUCKET_URL + 'media/'
    DEFAULT_FILE_STORAGE = 'myopedlist.utilities.s3.MediaRootS3BotoStorage'
# ------
   # PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/1.9/howto/static-files/
    #STATIC_ROOT = BASE_DIR.ancestor(1).child('staticfiles')
    #STATIC_URL = '/static/'

    # Extra places for collectstatic to find static files.
    #STATICFILES_DIRS = [
     #  BASE_DIR.child('static')
    #]
# -------
#    STATIC_ROOT = BASE_DIR.ancestor(1).child('staticfiles')
#    STATIC_URL = '/static/'
    #STATIC_URL = AWS_BUCKET_URL + 'static/'
#    STATIC_URL = 'https://myopedlist.s3.amazonaws.com:443/staticfiles/'#AWS_BUCKET_URL# + 'static/'
#    STATIC_URL = 'https://{0}.s3.amazonaws.com/static/'.format(
 #       AWS_STORAGE_BUCKET_NAME)
    #COMPRESS_ROOT = 'staticfiles'
    #COMPRESS_URL = STATIC_URL
    # AWS_S3_CUSTOM_DOMAIN = AWS_STORAGE_BUCKET_NAME + '.s3.amazonaws.com/'
    #COMPRESS_OUTPUT_DIR = ('CACHE')
    #STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
#    STATICFILES_STORAGE = 'myopedlist.utilities.s3.StaticRootS3BotoStorage'
    STATIC_URL = '/static/'
    STATIC_ROOT = 'staticfiles/javascript'
    COMPRESS_ROOT = 'staticfiles'
    COMPRESS_URL = STATIC_URL
