from .base import *


DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = ['*']
STATIC_URL = '/static/'
STATIC_ROOT = 'staticfiles'
COMPRESS_ROOT = 'staticfiles'
COMPRESS_OFFLINE = not DEBUG
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'myopedlist/db.sqlite3',
    }
}
