from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from qhonuskan_votes.models import (VotesField, ObjectsWithScoresManager,
                                    SortByScoresManager)


class Artist(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Anime(models.Model):
    title = models.CharField(max_length=200, unique=True)
    myanimelist_id = models.PositiveIntegerField(null=True, blank=True, unique=True)

    def __str__(self):
        return self.title


class Song(models.Model):
    title = models.CharField(max_length=200)
    #No data  genre = models.ManyToManyField(Genre, null=True, blank=True)
    thumb_url = models.URLField(default='https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/300px-No_image_available.svg.png')
    overview = models.TextField(null=True, blank=True)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    anime = models.ManyToManyField(Anime, related_name='oped_anime')
    OPED_CHOICES = (('OP', _('Opening')), ('ED', _('Ending')))
    oped = models.CharField(max_length=2, choices=OPED_CHOICES,
                            verbose_name=_('OP/ED'))
    number = models.PositiveSmallIntegerField(null=True, blank=True)
    #custom field (from-to episode, or nothing, which means all episodes)

    class Meta:
        unique_together = ('title', 'artist')

    def __str__(self):
        return self.title


class ListEntry(models.Model):
    SCORE_CHOICES = (
        (10, _('(10) Masterpiece')),
        (9, _('(9) Great')),
        (8, _('(8) Very good')),
        (7, _('(7) Good')),
        (6, _('(6) Fine')),
        (5, _('(5) Average')),
        (4, _('(4) Bad')),
        (3, _('(3) Very bad')),
        (2, _('(2) Horrible')),
        (1, _('(1) Appalling')),
    )
    user = models.ForeignKey(User)
    song = models.ForeignKey(Song)
    FAVORED_CHOICES = (
        (True, '❤'), (False, '-'),
    )
    favored = models.NullBooleanField(_('Favored'),
                                      choices=FAVORED_CHOICES,
                                      blank=True)
    score = models.PositiveSmallIntegerField(_('Score'),
                                             choices=SCORE_CHOICES,
                                             null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'list entries'
        unique_together = ('user', 'song')

    def __str__(self):
        return "{0}'s entry for {1}".format(self.user.username,
                                            self.song_id)


class SongReview(models.Model):
    entry = models.OneToOneField(ListEntry)
    text = models.TextField(_('Text'))
    date_created = models.DateTimeField(auto_now_add=True)
    votes = VotesField()
    objects = models.Manager()
    objects_with_scores = ObjectsWithScoresManager()
    sort_by_score = SortByScoresManager()

    def __str__(self):
        return "{0}'s review for {1}".format(self.entry.user.username,
                                             self.entry.song_id)

"""
class SongRecommendation(models.Model):
    entry1 = models.ForeignKey(ListEntry, related_name='recommendation_entry1')
    entry2 = models.ForeignKey(ListEntry, related_name='recommendation_entry2')
    text = models.TextField(_('Text'))
    date_created = models.DateTimeField(auto_now_add=True)
    votes = VotesField()
    objects = models.Manager()
    objects_with_scores = ObjectsWithScoresManager()
    sort_by_score = SortByScoresManager()

    class Meta:
        unique_together = ('entry1', 'entry2')

    def __str__(self):
        return "{0}'s recommendation for {1} - {2}".format(
            self.entry1.user.username,
            self.entry1.song_id,
            self.entry2.song_id)

    def save(self, *args, **kwargs):
        if self.entry1.song_id > self.entry2.song_id:
            self.entry1, self.entry2 = self.entry2, self.entry1
        super(SongRecommendation, self).save(*args, **kwargs)
"""

