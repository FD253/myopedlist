from datetime import timedelta

from django.core.urlresolvers import reverse
from django.db.models import Count, Avg
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone, translation
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from .forms import ListEntryForm, SongReviewForm
from .models import ListEntry, SongReview, Song, User, Anime
from ..utilities.mixins import LoginRequiredMixin, PermissionMixin, EntryMixin
#from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


def home(request):
    last_month = timezone.now().date() - timedelta(days=30)
    popular_last_month = ListEntry.objects.filter(date_created__gt=last_month) \
                    .values('song_id', 'song__title', 'song__anime__title',
                            'song__thumb_url') \
                    .annotate(average=Avg('score'), count=Count('song_id'))[:5]
    reviews = SongReview.objects.order_by('-date_created')[:5]
    best_last_month = ListEntry.objects.filter(date_created__gt=last_month) \
                    .values('song_id', 'song__title', 'song__anime__title',
                            'song__thumb_url') \
                    .annotate(average=Avg('score')).order_by('-average')
    context = dict(popular_last_month=popular_last_month, reviews=reviews,
                   best_last_month=best_last_month)
    return render(request, 'index.html', context)


class SongDetailView(DetailView):
    model = Song
    template_name = 'songs/song_detail.html'
    slug_field = 'id'

#   def dispatch(self, request, *args, **kwargs):
 #       self.song = Song.objects.get(id=kwargs['slug'])
  #      return redirect('songs:song_detail', kwargs={'id': self.song.id})

    def get_object(self):
        from django.http import Http404
        try:
            return super(SongDetailView, self).get_object()
        except Http404:
            return redirect('home')
#            return Song.objects.get_or_create(self.kwargs.get('slug'))

    def get_context_data(self, **kwargs):
        context = super(SongDetailView, self).get_context_data(**kwargs)
        song = self.object
        reviews = SongReview.objects.filter(
            entry__song__id=song.id).order_by('-date_created')[:3]
        context.update(dict(reviews=reviews, detail_page=True))
        return context


class SongListByUserView(ListView):
    model = ListEntry
    template_name = 'songs/song_list_by_user.html'

    def get_queryset(self):
        self.user_profile = get_object_or_404(
            User, username=self.kwargs['slug'])
        return ListEntry.objects.filter(user=self.user_profile)

    def get_context_data(self, **kwargs):
        context = super(SongListByUserView, self).get_context_data(**kwargs)
        context['user_profile'] = self.user_profile
        lang_info = translation.get_language_info(translation.get_language())
        context['lang'] = lang_info.get('name')
        return context


class ListEntryCreateView(LoginRequiredMixin, CreateView):
    model = ListEntry
    form_class = ListEntryForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.song = Song.objects.get(id=kwargs['slug'])
        try:
            entry = ListEntry.objects.get(user=request.user,
                                          song_id=self.song.id)
            return redirect(reverse('entry_update', kwargs={'pk': entry.pk, }))
        except ListEntry.DoesNotExist:
            pass
        return super(ListEntryCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.song = self.song
        return super(ListEntryCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('song_list_by_user',
                       kwargs={'slug': self.request.user.username})

    def get_context_data(self, **kwargs):
        context = super(ListEntryCreateView, self).get_context_data(**kwargs)
        context['song'] = self.song
        return context


class ListEntryUpdateView(PermissionMixin, UpdateView):
    model = ListEntry
    form_class = ListEntryForm

    def get_success_url(self):
        return reverse('song_list_by_user',
                       kwargs={'slug': self.request.user.username})


class ListEntryDeleteView(PermissionMixin, DeleteView):
    model = ListEntry

    def get_success_url(self):
        return reverse('song_list_by_user',
                       kwargs={'slug': self.request.user.username, })


class SongReviewCreateView(EntryMixin, CreateView):
    model = SongReview
    form_class = SongReviewForm

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            try:
                review = SongReview.objects.get(
                    entry__song__id=kwargs['slug'],
                    entry__user=request.user.id)
                return redirect(reverse('review_update',
                                        kwargs={'pk': review.pk}))
            except SongReview.DoesNotExist:
                self.song = Song.objects.get(id=kwargs.get('slug'))
        return super(SongReviewCreateView, self).dispatch(request,
                                                          *args, **kwargs)

    def form_valid(self, form):
        form.instance.entry = self.entry
        return super(SongReviewCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SongReviewCreateView, self).get_context_data(**kwargs)
        context['song'] = self.song
        return context

    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'slug': self.request.user.username})


class SongReviewUpdateView(PermissionMixin, UpdateView):
    model = SongReview
    form_class = SongReviewForm

    def get_success_url(self):
        return reverse('users:reviews',
                       kwargs={'slug': self.request.user.username})


class SongReviewDeleteView(PermissionMixin, DeleteView):
    model = SongReview

    def get_success_url(self):
        return reverse('users:reviews',
                       kwargs={'slug': self.request.user.username})


class SongReviewBySongView(ListView):
    model = SongReview
    template_name = 'songs/song_detail.html'

    def get_queryset(self):
        self.song = get_object_or_404(Song, id=self.kwargs['slug'])
        return SongReview.objects.all().filter(entry__song_id=self.song.id)

    def get_context_data(self, **kwargs):
        context = super(SongReviewBySongView, self).get_context_data(**kwargs)
        context['object'] = self.song
        context['reviews_page'] = True
        return context


class SongListByAnimeView(ListView):
    model = Song
    template_name = 'songs/song_list_by_anime.html'
    paginate_by = 20

    def get_queryset(self):
        self.anime = get_object_or_404(Anime, id=self.kwargs['slug'])
        return Song.objects.all().filter(anime__id=self.anime.id)

    def get_context_data(self, **kwargs):
        context = super(SongListByAnimeView, self).get_context_data(**kwargs)
        context['anime'] = self.anime
        return context


class TopSongsView(ListView):
    model = ListEntry
    template_name = 'songs/top_songs.html'
    paginate_by = 15

    def get_queryset(self):
        q = ListEntry.objects.filter(score__isnull=False)

        return q.values('song__id', 'song__title', 'song__thumb_url') \
                .annotate(average=Avg('score'), count=Count('song_id')) \
                .order_by('-average')

    def get_context_data(self, **kwargs):
        context = super(TopSongsView, self).get_context_data(**kwargs)
        return context
