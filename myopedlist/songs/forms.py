from django import forms
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from .models import *
from crispy_forms.layout import Submit, Reset
from crispy_forms.helper import FormHelper


class ListEntryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ListEntryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', _('Save')))
        self.helper.add_input(Reset('list_entry_reset',
                                    _('Reset'), css_class='btn-default'))

    class Meta:
        model = ListEntry
        fields = ('score',)


class SongReviewForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SongReviewForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', _('Submit')))

    class Meta:
        model = SongReview
        exclude = ('entry',)


class ListEntryChoiceField(forms.ModelChoiceField):

    def label_from_instance(self, obj):
        return obj.song.title
