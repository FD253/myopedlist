from django.contrib import admin
from .models import *

admin.site.register(ListEntry)
admin.site.register(SongReview)

admin.site.register(Song)
admin.site.register(Anime)
admin.site.register(Artist)
