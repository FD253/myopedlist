from django import template
from django.templatetags.static import static

from friendship.models import FriendshipManager

from myopedlist.songs.models import ListEntry, SongReview

register = template.Library()


@register.simple_tag
def user_is_friend(user_profile, request_user):
    if request_user.is_authenticated():
        fm = FriendshipManager()
        return fm.are_friends(user_profile, request_user)
    return False


@register.simple_tag
def avatar_or_default(user):
    avatar = user.userprofile.avatar
    if avatar:
        return avatar.url
    else:
        return static('img/default.png')


@register.simple_tag
def user_song_entry(user, song_id):
    if user.is_authenticated():
        try:
            return ListEntry.objects.get(user=user,
                                         song_id=song_id)
        except ListEntry.DoesNotExist:
            pass
    return

@register.simple_tag
def user_song_review(user, song_id):
    if user.is_authenticated():
        try:
            return SongReview.objects.get(user=user,
                                          song_id=song_id)
        except SongReview.DoesNotExist:
            pass
    return
