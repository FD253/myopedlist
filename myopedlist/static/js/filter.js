$(".discounted-item")
        .css("opacity","0.8")
       .hover(function(){
           $(this).css("opacity","1");
       }, function() {
           $(this).css("opacity","0.8");
});

$(function(){
   $("#allcat").click(function(){
       $(".discounted-item").slideDown();
       $("#catpicker a").removeClass("current");
       $("#catpicker a").attr( 'disabled', false);
       $(this).attr( 'disabled', true);
       $(this).addClass("current");
       return false;
   });
   
   $(".filter").click(function(){
        var thisFilter = $(this).attr("id");
        $(".discounted-item").slideUp();
        $("."+ thisFilter).slideDown();
        $("#catpicker a").removeClass("current");
        $("#catpicker a").attr( 'disabled', false);
        $(this).attr( 'disabled', true);
        $(this).addClass("current");
        return false;
   });
});