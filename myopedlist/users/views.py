from django.views.generic.edit import UpdateView
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView
from social.apps.django_app.default.models import UserSocialAuth

from myopedlist.users.forms import UserProfileForm
from ..utilities.mixins import LoginRequiredMixin
from ..songs.models import SongReview, ListEntry, Song
from ..users.models import User, UserProfile


class UserDetailView(DetailView):
    model = User
    template_name = 'users/user_detail.html'
    slug_field = 'username'

    def get_context_data(self, section=None, *args, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        reviews = SongReview.objects.filter(entry__user=self.object).order_by('-date_created')
        #recommendations -> PlayLists = SongRecomendation.objects_with_scores.filter(entry1__user=self.object).distinct().order_by('-date_created')
#        section = self.kwargs['section'] why this does not work?
        section = self.kwargs.get('section')
        if section == 'reviews':
            context['reviews_page'] = True
        else:
            context['detail_page'] = True
            reviews = reviews[:3]
            context['updates'] = ListEntry.objects.filter(user=self.object).order_by('-last_modified')[:3]
        context['reviews'] = reviews
        return context


class UserReviewsView(ListView):
    model = SongReview
    template_name = 'users/user_detail.html' #other that extends it

    def get_queryset(self):
        self.user_profile = get_object_or_404(User, username=self.kwargs['slug'])
        return self.model.objects.filter(entry__user=self.user_profile)

    def get_context_data(self, **kwargs):
        context = super(UserReviewsView, self).get_context_data(**kwargs)
        context['object'] = self.user_profile
        context['reviews'] = self.object_list
        context['reviews_page'] = True
        return context


class UserPlaylistView(ListView):
    pass


class UserProfileUpdate(LoginRequiredMixin, UpdateView):

    model = UserProfile
    form_class = UserProfileForm
    template_name = 'users/userprofile_form.html'

    def get_object(self):
        return self.request.user.profile

    def get_context_data(self, **kwargs):
        context = super(UserProfileUpdate, self).get_context_data(**kwargs)
        steam_account_exists = UserSocialAuth.objects.filter(
            provider='steam',
            user=self.request.user).exists()
        context['steam_account_exists'] = steam_account_exists
        return context
