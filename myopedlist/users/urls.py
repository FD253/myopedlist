from django.conf.urls import url

from .views import UserDetailView, UserReviewsView, UserProfileUpdate

app_name = 'users'
urlpatterns = [
    url(r'^(?P<slug>[\w-]+)/$', UserDetailView.as_view(), name='detail'),
    url(r'^(?P<slug>[\w-]+)/reviews/$', UserReviewsView.as_view(),
        name='reviews'),
    url(r'^profile/edit/$', UserProfileUpdate.as_view(), name='profile_update'),
]

